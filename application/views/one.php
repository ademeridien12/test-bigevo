<?php $this->load->view('templates/main');?>
    <div class="content-wrapper px-4 py-2">
        <h5 class="mt-3 mb-3">Soal Pertama</h5>
        <form class="row" action="<?=base_url('implement/processone');?>" method="post">
            <div class="col-lg-6 col-sm-12">
                <p class="text-secondary">Input</p>
                <div class="form-group mb-3">
                    <input type="text" class="form-control" id="masukan" name="masukan" value="<?=$this->session->flashdata('input');?>" placeholder="Masukkan String (Default : aaabbcccaaaac)">
                </div>
            </div>
            <div class="col-12">
                <button type="submit" class="btn btn-success btn-lg text-white">Proses</button>
            </div>
        </form>
        <div class="col-lg-6 col-sm-12 mt-3">
            <p class="text-secondary">Output</p>
            <p><?=$this->session->flashdata('output');?></p>
            <?php $this->session->set_flashdata('output', '');?>
            <?php $this->session->set_flashdata('input', '');?>
        </div>
    </div>
</div>

    <?php $this->load->view('templates/script');?>
</body>
</html>