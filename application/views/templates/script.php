<div id="sidebar-overlay"></div>

<!-- jQuery -->
<script src="<?=base_url('vendor/almasaeed2010/adminlte/plugins/jquery/jquery.min.js');?>"></script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('vendor/almasaeed2010/adminlte/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
<!-- Overlay Scrollbars -->
<script src="<?=base_url('vendor/almasaeed2010/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>
<!-- SWEETALERT -->
<script src="<?=base_url('vendor/almasaeed2010/adminlte/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('vendor/almasaeed2010/adminlte/dist/js/adminlte.min.js');?>"></script>

<script>
		$(document).ready(function() {
            $('#sidebar-overlay').click(function() {
                $('body').removeClass('sidebar-open').addClass('sidebar-closed sidebar-collapse');
            });
        });

        const flashMsg = '<?=$this->session->flashdata('message');?>';
        if (flashMsg) {
            Swal.fire({
                position: 'top-end',
                title: flashMsg,
                icon: 'info',
                showConfirmButton: false,
                timer: 2000
            });
            <?=$this->session->set_flashdata('message', '');?>
        }

        function logout() {
            Swal.fire({
                title: 'Logout',
                text: 'Kamu yakin ingin logout?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#D33',
                confirmButtonText: 'Logout',
                cancelButtonText: 'batal'
            }).then((res) => {
                if (res.isConfirmed) {
                    window.location.href = "<?=base_url('auth/logout');?>";
                }
            })
        }
</script>