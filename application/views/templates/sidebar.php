<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="#" class="brand-link">
      <span class="brand-text font-weight-light">Sistem Login</span>
  </a>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
          <a href="<?=base_url('/');?>" class="nav-link <?=($this->uri->segment(1) == '' || $this->uri->segment(1) == 'home') ? 'active' : '' ;?>">
            <i class="fas fa-home"></i>
            <p>
              Beranda
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="" class="nav-link <?=$this->uri->segment(1) == 'implement' ? 'active' : '';?>">
            <i class="far fa-list-alt"></i>
            <p>
              Implementasi Soal
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?=base_url('implement/one');?>" class="nav-link <?=$this->uri->segment(2) == 'one' ? 'active' : '';?>">
                <i class="fas fa-asterisk nav-icon"></i>
                <p>Soal Pertama</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?=base_url('implement/two');?>" class="nav-link <?=$this->uri->segment(2) == 'two' ? 'active' : '';?>">
                <i class="fas fa-asterisk nav-icon"></i>
                <p>Soal Kedua</p>
              </a>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>