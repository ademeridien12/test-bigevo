<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Skill Test Big Evo - Ade Meridien Jaya</title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('vendor/almasaeed2010/adminlte/plugins/fontawesome-free/css/all.min.css');?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?=base_url('vendor/almasaeed2010/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('vendor/almasaeed2010/adminlte/dist/css/adminlte.min.css');?>">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?=base_url('vendor/almasaeed2010/adminlte/plugins/sweetalert2/sweetalert2.min.css');?>">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url('vendor/almasaeed2010/adminlte/plugins/toastr/toastr.min.css');?>">
    <!-- CHOSEN CSS -->
    <link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Hi <?=$this->session->userdata('test_bigevo_user')['fullname'];?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown2">
                        <a class="dropdown-item text-danger" onclick="logout()"><i class="fas fa-sign-out-alt"></i> Sign Out</a>
                    </div>
                </li>
            </ul>
        </nav>
        <?php $this->load->view('templates/sidebar');?>