<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Test BigEvo - Daftar</title>
        <meta charseg="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendors/bootstrap/css/bootstrap.min.css');?>">
        <!-- Font Awesome -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css');?>">
        <!-- Animate -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendors/animate/animate.css');?>">
        <!-- Hamburgers -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendors/css-hamburgers/hamburgers.min.css');?>">
        <!-- Select2 -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('assets/vendors/select2/select2.min.css');?>">
        <!-- Base -->
        <link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/util.css');?>">
        <link rel="stylesheet" type="text/css" href="<?=base_url('assets/login/main.css');?>">
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div class="login100-pic js-tilt" data-tilt>
                        <img src="<?=base_url('assets/login/images/img-01.png');?>" alt="IMG">
                    </div>

                    <form class="login100-form validate-form" action="<?=base_url('auth/registerdata');?>" method="POST">
                        <span class="login100-form-title">
                            Buat Akun
                        </span>

                        <div class="wrap-input100 validate-input" data-validate="Nama lengkap dibutuhkan">
                            <input class="input100" type="text" name="fullname" placeholder="Nama Lengkap">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-address-card" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Email dibutuhkan">
                            <input class="input100" type="email" name="email" placeholder="Email">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-at" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Password dibutuhkan">
                            <input class="input100" type="password" name="password" placeholder="Password">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Konfirmasi password dibutuhkan">
                            <input class="input100" type="password" name="confirmpass" placeholder="Konfirmasi Password">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="container-login100-form-btn">
                            <button type="submit" class="login100-form-btn">
                                Daftar
                            </button>
                        </div>

                        <div class="text-center p-t-136">
                            <a class="txt2" href="<?=base_url('auth');?>">
                                <i class="fa fa-long-arrow-left m-l-5" aria-hidden="true"></i>
                                Login
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- JQuery -->
        <script src="<?=base_url('assets/vendors/jquery/jquery-3.2.1.min.js');?>"></script>
        <!-- Bootstrap -->
        <script src="<?=base_url('assets/vendors/bootstrap/js/popper.js');?>"></script>
        <script src="<?=base_url('assets/vendors/bootstrap/js/bootstrap.min.js');?>"></script>
        <!-- Select2 -->
        <script src="<?=base_url('assets/vendors/select2/select2.min.js');?>"></script>
        <!-- JQuery Tilt -->
        <script src="<?=base_url('assets/vendors/tilt/tilt.jquery.min.js');?>"></script>
        <!-- Base -->
        <script src="<?=base_url('assets/login/main.js');?>"></script>
        <!-- SweetAlert -->
        <script src="<?=base_url('vendor/almasaeed2010/adminlte/plugins/sweetalert2/sweetalert2.all.min.js');?>"></script>

        <script>
            $('.js-tilt').tilt({
                scale: 1.1
            });
            const flashMsg = '<?=$this->session->flashdata('message');?>';
            if (flashMsg) {
                Swal.fire({
                    position: 'top-end',
                    title: flashMsg,
                    icon: 'info',
                    showConfirmButton: false,
                    timer: 2000
                });
                <?=$this->session->set_flashdata('message', '');?>
            }
        </script>
    </body>
</html>