<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('test_bigevo_user')) {
            $url = base_url('auth');
            redirect($url);
        }
    }

    public function index() {
        $this->load->view('home');
    }
}