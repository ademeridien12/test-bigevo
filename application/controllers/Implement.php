<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Implement extends CI_Controller {
    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('test_bigevo_user')) {
            $url = base_url('auth');
            redirect($url);
        }
    }

    public function one() {
        $this->load->view('one');
    }

    public function two() {
        $this->load->view('two');
    }

    public function processone() {
        $data = $this->input->post('masukan');

        if (empty($data)) {
            $data = "aaabbcccaaaac";
        }

        $input= str_split($data);

        $id = 1;
        $c = 1;

        for ($idx = 0; $idx < count($input); $idx++) {
            if ($idx == 0) {
                @$val[$id][$input[$idx]]++;
                $c = 1;
            } else {
                if (array_key_exists($input[$idx], $val[$id-$c])) {
                    $val[$id-$c][$input[$idx]]++;
                    $c++;
                } else {
                    @$val[$id][$input[$idx]]++;
                    $c = 1;
                }
            }
            $id++;
        }

        $output = "";

        foreach($val as $v) {
            $key = array_keys($v);
            $sum = array_values($v);
            
            for($i=0; $i < count($key); $i++) {
                $output .= $key[$i] . " = " . $sum[$i] . "<br/>";
            }
        }

        $this->session->set_flashdata('input', $data);
        $this->session->set_flashdata('output', $output);
        $this->session->set_flashdata('message', 'Silahkan lihat di bagian output');
        redirect('implement/one');
    }

    public function processtwo() {
        $data = $this->input->post('masukan');

        if (empty($data)) {
            $data = "SeLamAT PAGi semua halOo";
        }

        $output = "Format Judul = " . ucwords(strtolower($data)) . "<br/>Format Biasa = " . ucwords(strtolower($data), 1);

        $this->session->set_flashdata('input', $data);
        $this->session->set_flashdata('output', $output);
        $this->session->set_flashdata('message', 'Silahkan lihat di bagian output');
        redirect('implement/two');
    }
}