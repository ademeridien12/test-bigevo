<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('M_user', 'm_user');
    }

    public function index() {
        if ($this->session->has_userdata('test_bigevo_user')) {
            redirect('home');
        }
        $this->load->view('login');
    }

    public function register() {
        if ($this->session->has_userdata('test_bigevo_user')) {
            redirect('home');
        }
        $this->load->view('register');
    }

    public function registerdata() {
        $fullname = $this->input->post('fullname');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $confirmpass = $this->input->post('confirmpass');
        
        if ($password != $confirmpass) {
            $this->session->set_flashdata('message', 'Password & konfirmasi password tidak sama.');
            redirect('auth/register');
        } else {
            $regdata = [
                'fullname' => $fullname,
                'email' => $email,
                'password' => md5($password)
            ];
            $reg = $this->m_user->addData($regdata);
            if ($reg == 'success') {
                $this->session->set_flashdata('message', 'Pendaftaran berhasil, sekarang silahkan login');
                redirect('auth');
            } else if ($reg == 'duplicate') {
                $this->session->set_flashdata('message', 'Pendaftaran gagal. Email telah digunakan. Mohon gunakan email lain.');
                redirect('auth/register');
            } else {
                $this->session->set_flashdata('message', 'Pendaftaran gagal. Server sedang sibuk. Silahkan coba lagi');
                redirect('auth/register');
            }
        }
    }

    public function login() {
        $email = $this->input->post('email');
        $password = $this->input->post('password');

        $cekdata = ['email' => $email];
        $cek = $this->m_user->getRow($cekdata);
        if ($cek) {
            if ($cek->password == md5($password)) {
                $session = [
                    'id' => $cek->id,
                    'fullname' => $cek->fullname,
                    'email' => $cek->email
                ];
                $this->session->set_userdata('test_bigevo_user', $session);
                redirect('/');
            } else {
                $this->session->set_flashdata('message', 'Password salah');
                redirect('/');
            }
        } else {
            $this->session->set_flashdata('message', 'User tidak ditemukan');
            redirect('/');
        }
    }

    public function logout() {
        $this->session->unset_userdata('test_bigevo_user');
        $this->session->set_flashdata('message', 'Sukses logout');
        redirect('/');
    }
}