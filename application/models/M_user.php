<?php
class M_user extends CI_Model {
    
    public function getData() {
        $this->db->select('*');
        $user = $this->db->get('user');
        return $user->result();
    }

    public function addData($data) {
        try {
            $this->db->trans_start();
            $this->db->insert('user', $data);
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE) {
                return 'fail';
            } else {
                return 'success';
            }
        } catch(Exception $e) {
            return 'duplicate';
        }
    }

    public function getRow($field) {
        $this->db->select('*');
        $this->db->where($field);
        $user = $this->db->get('user');
        return $user->row();
    }
}